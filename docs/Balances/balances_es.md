# Balances en Cuentas y Tarjetas

## Qué tipos de balances existen

### Balances para Cuentas y Tarjetas

Dependiendo del tipo, una cuenta/tarjeta puede tener varios balances significativos:

   * Aplica para todos los tipos de cuentas/tarjetas:
      + **Balance disponible real:** indica la cantidad en tiempo real que podemos destinar a realizar operaciones. Se actualiza en tiempo real, es decir, si se realiza un gasto, este balance se actualiza al momento de realizarlo.
         * En el caso de las _tarjetas de débito_, este balance es, en realidad, el balance correspondiente a la cuenta a la cual se cargan las operaciones realizadas con la tarjeta, por lo que realmente no es un balance de la tarjeta.
      + **Balance disponible contable:** indica la cantidad contable que disponemos en nuestra cuenta/tarjeta para realizar operaciones. Se diferencia del balance real en que este balance se actualiza cuando el movimiento se contabiliza, lo cual puede suceder varios días después de haber realizado el movimiento.
         * En el caso de las _tarjetas de débito_, este balance es, en realidad, el balance correspondiente a la cuenta a la cual se cargan las operaciones realizadas con la tarjeta, por lo que realmente no es un balance de la tarjeta.
	     * En el caso de las _tarjetas prepago_ este balance no existe, puesto que no hay proceso de contabilización.
   * Sólo para cuentas/tarjetas de crédito:
      + **Crédito concedido:** es el crédito concedido a la cuenta/tarjeta. Indica la cantidad máxima prestada por el banco de la que se puede disponer a través de esa cuenta/tarjeta. Este valor permanece invariable, salvo que se negocie explícitamente.
      + **Balance dispuesto real:** indica la cantidad en tiempo real que hemos consumido de nuestro crédito concedido. Se actualiza en tiempo real, es decir, si se realiza un gasto, este balance se actualiza al momento de realizarlo.
      + **Balance dispuesto contable:** indica la cantidad contable que hemos consumido de nuestro crédito concedido. Se diferencia del balance real en que este balance se actualiza cuando el movimiento se contabiliza, lo cual puede suceder varios días después de haber realizado el movimiento.

### Cuentas/Tarjetas de crédito: Qué relación hay entre sus balances

Como vemos, las cuentas y tarjetas de crédito tienen unos balances adicionales con respecto a las que no son de crédito. La correlación entre estos balances se muestra en el siguiente ejemplo con una tarjeta de crédito, aunque vale para el caso de las cuentas de crédito:

   * **Día 1**: Contratamos una tarjeta de crédito con un crédito concedido de 3.000€.
      + En este momento, los balances muestran los siguientes valores:
         * _Crédito concedido_ -> 3.000€
         * _Balance disponible real_ -> 3.000€
         * _Balance disponible contable_ -> 3.000€
         * _Balance dispuesto real_ -> 0€
         * _Balance dispuesto contable_ -> 0€
      + Realizamos una retirada en cajero de 150€, quedando los balances de la siguiente manera:
         * _Crédito concedido_ -> 3.000€
         * _Balance disponible real_ -> **2.850€**
         * _Balance disponible contable_ -> 3.000€
         * _Balance dispuesto real_ -> **150€**
         * _Balance dispuesto contable_ -> 0€
   * **Día 2**: Supongamos que el proceso de contabilización de movimientos se hace a D+1.
      + Al comienzo del día, los balances estarían así:
         * _Crédito concedido_ -> 3.000€
         * _Balance disponible real_ -> 2.850€
         * _Balance disponible contable_ -> **2.850€**
         * _Balance dispuesto real_ -> 150€
         * _Balance dispuesto contable_ -> **150€**
      + Realizamos una compra por valor de 100€, quedando de la siguiente manera:
         * _Crédito concedido_ -> 3.000€
         * _Balance disponible real_ -> **2.750€**
         * _Balance disponible contable_ -> 2.850€
         * _Balance dispuesto real_ -> **250€**
         * _Balance dispuesto contable_ -> 150€
      + Realizamos una compra por valor de 50€, quedando de la siguiente manera:
         * _Crédito concedido_ -> 3.000€
         * _Balance disponible real_ -> **2.700€**
         * _Balance disponible contable_ -> 2.850€
         * _Balance dispuesto real_ -> **300€**
         * _Balance dispuesto contable_ -> 150€	  
   * **Día 3**:
      + Al comienzo del día, las operaciones se contabilizan, actualizándose a los siguientes valores:
         * _Crédito concedido_ -> 3.000€
         * _Balance disponible real_ -> 2.700€
         * _Balance disponible contable_ -> **2.700€**
         * _Balance dispuesto real_ -> 300€
         * _Balance dispuesto contable_ -> **300€**
	  
Como vemos en el ejemplo, el comportamiento que se observa es el siguiente:

   * _Crédito concedido_ no varía nunca
   * _Balance disponible real_ disminuye con cada gasto en el mismo momento de realizarse
   * _Balance disponible contable_ disminuye con cada gasto cuando se realiza la contabilización
   * _Balance dispuesto real_ aumenta con cada gasto en el mismo momento de realizarse
   * _Balance dispuesto contable_ aumenta con cada gasto cuando se realiza la contabilización
	  
Observando el ejemplo, también vemos que siempre se cumplen estas fórmulas:

   * _Balance disponible real_ + _Balance dispuesto real_ = _Crédito concedido_
   * _Balance disponible contable_ + _Balance dispuesto contable_ = _Crédito concedido_

## Cómo se reflejan estos balances en las APIs

Como podemos ver, tanto cuentas como tarjetas pueden tener los mismos balances y la forma de reflejarlos en las APIs es la misma en ambos casos. Haciendo referencia a los términos usados, vamos a ver cómo se reflejan en las APIs:

   * **availableBalance**: dado que tenemos dos balances disponibles distintos, real y contable, éstos se agrupan bajo un misma estructura denominada _available balance_. Esta estructura aglutina los dos balances disponibles mencionados, además de un tercero que refleja la diferencia entre ambos:
      + **currentBalances**: este balance se corresponde con el _balance disponible real_
      + **postedBalances**: este balance se corresponde con el _balance disponible contable_
      + **pendingBalances**: este balance refleja la diferencia entre _balance disponible real_ y _balance disponible contable_  (_availableBalance.currentBalances_ - _availableBalance.postedBalances_)
   * **grantedCredits**: el _Crédito concedido_, realmente, no es un balance, pero sirve para entender la parte de referida al _balance dispuesto_. Este valor se informa a través del atributo _grantedCredits_.
   * **disposedBalance**: al igual que con el _balance disponible_, tenemos dos balances distintos, real y contable, y también se agrupan bajo un misma estructura denominada _disposedBalance_. Esta estructura aglutina los dos balances dispuestos mencionados, además de un tercero que refleja la diferencia entre ambos:
      + **currentBalances**: este balance se corresponde con el _balance dispuesto real_
      + **postedBalances**: este balance se corresponde con el _balance dispuesto contable_
      + **pendingBalances**: este balance refleja la diferencia entre _balance dispuesto real_ y _balance dispuesto contable_ (_disposedBalance.currentBalances_ - _disposedBalance.postedBalances_)
   
## Por qué los balances son arrays

Los atributos que traen los balances se denominan en plural porque, realmente, son arrays. Pero, ¿qué sentido tiene que el valor de un balance sea un array? Esto es, básicamente, por dos motivos:

   * **Informar el contravalor en moneda local** cuando la cuenta o la tarjeta están contratados en una moneda distinta de la del país de contratación. Asociado al atributo _currencies_ donde se informan las currencies en las que vendrán informadas las cantidades, indicando con el atributo _isMajor_ a _true_ la currency en la que se contrató la cuenta/tarjeta.
   * **Dar cabida a las tarjetas de crédito de Chile** las cuales manejan dos créditos independientes dentro de una misma tarjeta:
      + Crédito en Pesos chilenos (CLP) para operaciones realizadas en moneda local
      + Crédito en US Dólares (USD) para operaciones realizadas en cualquier moneda extranjera.